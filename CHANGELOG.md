
# Changelog for VRE Deploy Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v4.6.0]

- maven-parent 1.2.0
- maven-portal-bom 4.0.0

## [v4.5.0] - 2022-03-08

- Increased waiting time between LR Site Creation and LR Site user assignments (manager, designer) [#24402]

## [v4.4.0] - 2021-07-08

- migrated to new AccessTokenProvider class [#21781]
- removed home library dep, using shub for messaging [#21157]

## [v4.3.1] - 2021-02-02

- Ported to git
- modified to support the new IAM Keycloak based

## [v1.0.0] - 2008-10-21

- First release, for changes between 1.0 version and 4.2 see changelog xml in distro folder.
