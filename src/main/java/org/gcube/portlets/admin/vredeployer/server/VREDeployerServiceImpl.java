package org.gcube.portlets.admin.vredeployer.server;

import static org.gcube.common.authorization.client.Constants.authorizationService;
import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.gcube.application.framework.core.session.ASLSession;
import org.gcube.application.framework.core.session.SessionManager;
import org.gcube.application.framework.vremanagement.vremanagement.impl.VREGeneratorEvo;
import org.gcube.applicationsupportlayer.social.ApplicationNotificationsManager;
import org.gcube.applicationsupportlayer.social.NotificationsManager;
import org.gcube.applicationsupportlayer.social.shared.SocialNetworkingSite;
import org.gcube.applicationsupportlayer.social.shared.SocialNetworkingUser;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.client.dsl.Util;
import org.gcube.common.storagehub.client.dsl.VREFolderManager;
import org.gcube.common.storagehub.client.plugins.AbstractPlugin;
import org.gcube.common.storagehub.client.proxies.MessageManagerClient;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.portal.custom.communitymanager.SiteManagerUtil;
import org.gcube.portal.custom.communitymanager.ThemesIdManager;
import org.gcube.portal.custom.communitymanager.components.GCUBESiteLayout;
import org.gcube.portal.custom.communitymanager.impl.GCubeSiteManagerImpl;
import org.gcube.portal.custom.scopemanager.scopehelper.ScopeHelper;
import org.gcube.portal.oidc.lr62.OIDCUmaUtil;
import org.gcube.portlets.admin.vredeployer.client.VredeployerService;
import org.gcube.portlets.admin.vredeployer.client.model.VREFunctionalityModel;
import org.gcube.portlets.admin.vredeployer.shared.ResourceCategory;
import org.gcube.portlets.admin.vredeployer.shared.ResourceCategoryItem;
import org.gcube.portlets.admin.vredeployer.shared.VREDeployerStatusType;
import org.gcube.portlets.admin.vredeployer.shared.VREDescrBean;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientCloudReport;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientDeployReport;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientFunctionalityDeployReport;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientFunctionalityReport;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientResource;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientResourceManagerDeployingReport;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientResourcesDeployReport;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.ClientServiceReport;
import org.gcube.portlets.admin.vredeployer.shared.deployreport.DeployStatus;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.RoleManager;
import org.gcube.vomanagement.usermanagement.UserManager;
import org.gcube.vomanagement.usermanagement.exception.GroupRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeRole;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.gcube.vomanagement.usermanagement.model.GatewayRolesNames;
import org.gcube.vremanagement.vremodel.cl.stubs.types.FunctionalityItem;
import org.gcube.vremanagement.vremodel.cl.stubs.types.GHN;
import org.gcube.vremanagement.vremodel.cl.stubs.types.GHNsPerFunctionality;
import org.gcube.vremanagement.vremodel.cl.stubs.types.ResourceDescriptionItem;
import org.gcube.vremanagement.vremodel.cl.stubs.types.ResourceItem;
import org.gcube.vremanagement.vremodel.cl.stubs.types.RunningInstanceMessage;
import org.gcube.vremanagement.vremodel.cl.stubs.types.VREDescription;
import org.gcube.vremanagement.vremodeler.utils.Utils;
import org.gcube.vremanagement.vremodeler.utils.reports.DeployReport;
import org.gcube.vremanagement.vremodeler.utils.reports.FunctionalityReport;
import org.gcube.vremanagement.vremodeler.utils.reports.Resource;
import org.gcube.vremanagement.vremodeler.utils.reports.ServiceReport;
import org.gcube.vremanagement.vremodeler.utils.reports.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Theme;
import com.liferay.portal.model.User;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;
import com.liferay.portal.service.ThemeLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;



/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings({ "deprecation", "serial" })
public class VREDeployerServiceImpl  extends RemoteServiceServlet implements VredeployerService {

	private static final Logger log = LoggerFactory.getLogger(VREDeployerServiceImpl.class);

	/**
	 * during the vre phase creation we need to update the Application Profile of Calendar and News feed
	 */
	private static final String CALENDAR_APPID = "org.gcube.portal.calendarwrapper.GCubeCalendarHandler";
	private static final String NEWS_FEED_APPID = "org.gcube.portlets.user.newsfeed.server.NewsServiceImpl";

	protected static final String ORGANIZATION_DEFAULT_LOGO = "/org/gcube/portal/custom/communitymanager/resources/default_logo.png";
	protected static final String ORGANIZATION_DEFAULT_LOGO_URL = "http://ftp.d4science.org/apps/profiles/d4slogo.png";
	public static final String AUTORISED_INFRA_ROLE = "Infrastructure-Manager";
	/**
	 * 
	 */
	private static final String VRE_GENERATOR_ATTRIBUTE = "VREGenerator";
	private static final String GHN_PER_FUNC_ATTRIBUTE = "GHN_PER_FUNC_ATTRIBUTE";

	private static final String HARD_CODED_VO_NAME = "/gcube/devsec";
	private static final String APPROVING_VRE = "approvingVRE";
	public static final String APPROVE_MODE = "approve";
	public static final String EDIT_MODE = "edit";
	public static final String REEDIT_TYPE_ATTRIBUTE = "reeditType";
	public static final String MODE_ATTRIBUTE = "mode";
	private static final String DESCRIPTION = "DESCRIPTION";
	private static final String DEPLOYING = "DEPLOYING";

	private static final String DESIGNER = "Designer";
	private static final String MANAGER = "Manager";

	private static final String VRE_MANAGER = "VRE-Manager";
	private static final String VRE_DESIGNER = "VRE-Designer";

	private static final int WAITING_TIME_Seconds = 24;
	/**
	 * 
	 */
	private boolean isTesting = true;
	String customEPR = "1d648460-2d05-11e3-9f6f-a17a856bd44f";
	/**
	 * 
	 * @return
	 */
	private VREGeneratorEvo getVREGeneratorEvo(ASLSession aslSession){
		log.info("getVREGeneratorEvo called with scope: " + aslSession.getScopeName());
		String vreid = (String) aslSession.getAttribute(VRE_GENERATOR_ATTRIBUTE);
		if(vreid==null){
			return null;
		}
		return new VREGeneratorEvo(aslSession, vreid); //cannot cache VREGeneretor instance;
	}

	private void setDeployingStatusOn() {
		getASLSession().setAttribute(DEPLOYING, "ON");
	}

	private void setDeployingStatusOff() {
		getASLSession().setAttribute(DEPLOYING, null);
	}

	private boolean isDeploying() {
		return getASLSession().getAttribute(DEPLOYING) != null;
	}

	/**
	 * 
	 * @return
	 */
	private ASLSession getASLSession() {
		log.info("getVREGeneratorEvo  getASLSession() : ");
		HttpSession session = this.getThreadLocalRequest().getSession();
		String username = (String) session.getAttribute(ScopeHelper.USERNAME_ATTRIBUTE);
		if (username == null) {
			username = "massimiliano.assante";
			SessionManager.getInstance().getASLSession(session.getId(), username).setScope(HARD_CODED_VO_NAME);
			SessionManager.getInstance().getASLSession(session.getId(), username).setAttribute(REEDIT_TYPE_ATTRIBUTE, APPROVE_MODE);
		}
		else {
			isTesting = false;
		}
		return SessionManager.getInstance().getASLSession(session.getId(), username);
	}

	/**
	 * {@inheritDoc}
	 */
	public VREDeployerStatusType isApprovingModeEnabled() {
		if (isTesting) {
			setDeployingStatusOff();
			return VREDeployerStatusType.APPROVE;

		}

		if (isDeploying())
			return VREDeployerStatusType.DEPLOYING;

		log.debug("---  isApprovingModeEnabled in Log   ---");

		ASLSession aslSession = getASLSession();
		aslSession.setAttribute(MODE_ATTRIBUTE, null);

		String reeditType = (String) aslSession.getAttribute(REEDIT_TYPE_ATTRIBUTE);
		log.debug("REEDIT_TYPE_ATTRIBUTE = " + reeditType);
		aslSession.setAttribute(REEDIT_TYPE_ATTRIBUTE, null);

		if ( reeditType != null && reeditType.compareTo(APPROVE_MODE) == 0) {
			System.out.println(APPROVING_VRE + " = " + true);
			aslSession.setAttribute(MODE_ATTRIBUTE,APPROVING_VRE);
			return VREDeployerStatusType.APPROVE;
		}
		else{
			/* This is needed to avoid to log out and log in again to create a new VRE */
			aslSession.setAttribute(VRE_GENERATOR_ATTRIBUTE, null);
		}
		aslSession.setAttribute(REEDIT_TYPE_ATTRIBUTE, null);

		return VREDeployerStatusType.NON_APPROVE;
	}


	/**
	 * return the VRE Overall Information
	 */
	public VREDescrBean getVRE() throws NullPointerException {
		VREGeneratorEvo vreGenerator = null;
		if (isTesting) {
			vreGenerator = new VREGeneratorEvo(getASLSession(), customEPR);
		}
		else {
			ASLSession aslSession = getASLSession();
			vreGenerator = getVREGeneratorEvo(aslSession);
			log.info("VRE EPR: " + vreGenerator.getVREepr());
		}		

		VREDescrBean vreDescBean = null;
		log.debug("---   Getting VRE Model   ---");
		try {
			VREDescription sd = vreGenerator.getVREModel();
			vreDescBean = new VREDescrBean(sd.name(), sd.description(), sd.designer(), sd.manager(), sd.startTime().getTime(), sd.endTime().getTime());
			getASLSession().setAttribute(DESIGNER,  sd.designer());
			getASLSession().setAttribute(MANAGER,   sd.manager());
		} catch (RemoteException e) {
			e.printStackTrace();
		}	
		log.debug("Model: " + vreDescBean.getName());
		log.debug("--- END Getting VRE Model ---");
		return vreDescBean;

	}

	/**
	 * read the available functionality from the service through ASL extension
	 */
	@Override
	public VREFunctionalityModel getFunctionality()  {

		VREFunctionalityModel toReturn = new VREFunctionalityModel("","selected functionality", "", "",false);

		HashMap<String, ArrayList<ResourceCategory>> funCategories = new HashMap<String, ArrayList<ResourceCategory>>();

		List<FunctionalityItem> list = null;
		VREGeneratorEvo vreGenerator = null;
		try {
			if (isTesting) 
				vreGenerator = new VREGeneratorEvo(getASLSession(), customEPR);
			else {
				ASLSession aslSession = getASLSession();
				vreGenerator = getVREGeneratorEvo(aslSession);
				log.info("VRE EPR: " + vreGenerator.getVREepr());
			}		
			list = vreGenerator.getFunctionality();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (list == null) {
			log.warn("FunctionalityList NULL");
			return null;
		} 

		for (FunctionalityItem fi :list){
			log.info(fi.id()+"-"+fi.name());

			VREFunctionalityModel toAdd = new VREFunctionalityModel(Integer.toString(fi.id()),
					fi.name(), fi.description(), "",fi.selected());

			if (fi.children() != null) {
				List<FunctionalityItem> children = fi.children();
				ArrayList<VREFunctionalityModel> newchildrens = new ArrayList<VREFunctionalityModel>();
				//creating node children
				for (FunctionalityItem child : children) {
					log.info(child.name() + " is " + child.selected());
					if (child.selected()) {
						VREFunctionalityModel subFunc = new VREFunctionalityModel(Integer.toString(child.id()),
								child.name(), child.description(),
								"functionality-add-icon",child.selected());
						newchildrens.add(subFunc);

						ArrayList<VREFunctionalityModel> resourceChildren = new ArrayList<VREFunctionalityModel>();
						if ( child.selectableResourcesDescription()!=null) {
							for (ResourceDescriptionItem category:  child.selectableResourcesDescription()) {
								if (category.resources()!=null) 
									for (ResourceItem resource : category.resources()) 
										if (resource.selected()) 
											resourceChildren.add(new VREFunctionalityModel(resource.id(), resource.name(), resource.description(), "extres-icon", resource.selected()));
							}
							//subFunc.addChildren(resourceChildrens.toArray(new VREFunctionalityModel[resourceChildrens.size()]));
						}
						AdditionalFuncInfo addInfo = getServicesAndGHNs(subFunc.getId());

						if (addInfo != null) {
							RunningInstanceMessage[] ris = addInfo.getMissingServices();

							for (int j = 0; j < ris.length; j++) {
								RunningInstanceMessage ri = ris[j];
								resourceChildren.add(new VREFunctionalityModel("", ri.serviceName() + " (" + ri.serviceClass()+")", "", "missing-ri", true));
							}
							ris = addInfo.getFoundServices();
							for (int j = 0; j < ris.length; j++) {
								RunningInstanceMessage ri = ris[j];
								resourceChildren.add(new VREFunctionalityModel("", ri.serviceName() + " (" + ri.serviceClass()+")", "", "runninginstance-icon", true));
							}

							GHN[] relGHNs = addInfo.getGhns();
							for (int j = 0; j < relGHNs.length; j++) {
								GHN ghn = relGHNs[j];
								resourceChildren.add(new VREFunctionalityModel("", ghn.host() + " (" + ghn.site().domain()+")", "", "architecture-icon", true));
							}
						}
						else
							log.error("getServicesAndGHNs per subfunctionality returns NULL");
						subFunc.addChildren(resourceChildren.toArray(new VREFunctionalityModel[resourceChildren.size()]));
					}
				}
				if (newchildrens.size() > 0) {
					toAdd.addChildren(newchildrens.toArray(new VREFunctionalityModel[newchildrens.size()]));
					toReturn.add(toAdd);
				}
			}	
		}

		for (String func : funCategories.keySet()) {
			log.debug("-"+func);
			for(ResourceCategory category : funCategories.get(func)) {
				log.debug("--"+category.getName());
				for(ResourceCategoryItem rc: category.getItems()) {
					log.debug("----"+rc.getName() + " : " + rc.isSelected());
				}
			}
		}
		return toReturn;
	}

	@Override
	public void getGHNPerFunctionality(String funcId) {
		VREGeneratorEvo vreGenerator = null;
		if (isTesting) 
			vreGenerator = new VREGeneratorEvo(getASLSession(), customEPR);
		else  {
			ASLSession aslSession = getASLSession();
			vreGenerator = getVREGeneratorEvo(aslSession);
		}
		int funcToLookFor = Integer.parseInt(funcId);

		try {
			GHNsPerFunctionality[] ghnsPerFunc = vreGenerator.getGHNsPerFunctionality();
			GHNsPerFunctionality toWorkWith = null;
			for (int i = 0; i < ghnsPerFunc.length; i++) 
				if (funcToLookFor == ghnsPerFunc[i].id()) {
					toWorkWith = ghnsPerFunc[i];
					break;
				}
			if (toWorkWith == null) 
				return;
			List<RunningInstanceMessage> services =  toWorkWith.foundServices();
			if (services != null) {
				for (RunningInstanceMessage se : services) {
					log.debug(se.serviceName() + " - " + se.serviceClass());
				}
			}

		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}


	private void setGHNsPerFunctionalityInSession(GHNsPerFunctionality[] ghnsPerFunc) {
		getASLSession().setAttribute(GHN_PER_FUNC_ATTRIBUTE, ghnsPerFunc);
	}

	private GHNsPerFunctionality[] getGHNsPerFunctionalityFromSession() {
		return (GHNsPerFunctionality[]) getASLSession().getAttribute(GHN_PER_FUNC_ATTRIBUTE);
	}
	/**
	 * get the list of running instances associated to a service
	 * @param funcId
	 * @return
	 */
	private AdditionalFuncInfo getServicesAndGHNs(String funcId) {
		AdditionalFuncInfo toReturn = new AdditionalFuncInfo();
		try {
			GHNsPerFunctionality[] ghnsPerFunc = null;

			if (getGHNsPerFunctionalityFromSession() == null) { //avoid multiple calls to the service
				VREGeneratorEvo vreGenerator = null;
				if (isTesting) 
					vreGenerator = new VREGeneratorEvo(getASLSession(), customEPR);
				else {
					ASLSession aslSession = getASLSession();
					vreGenerator = getVREGeneratorEvo(aslSession);
				}
				ghnsPerFunc = vreGenerator.getGHNsPerFunctionality();
				setGHNsPerFunctionalityInSession(ghnsPerFunc);
			}
			else {
				ghnsPerFunc = getGHNsPerFunctionalityFromSession();
			}
			int funcToLookFor = Integer.parseInt(funcId);
			GHNsPerFunctionality ghnPF = null;
			for (int i = 0; i < ghnsPerFunc.length; i++) {
				if (funcToLookFor == ghnsPerFunc[i].id()) {	
					ghnPF = ghnsPerFunc[i];	
					break;
				}
			}
			if (ghnPF == null || ghnPF.missingServices() == null) 
				toReturn.setMissingServices(new RunningInstanceMessage[0]);
			else {
				toReturn.setMissingServices(ghnPF.missingServices().toArray(new RunningInstanceMessage[0]));
			}
			if (ghnPF == null || ghnPF.foundServices() == null) 
				toReturn.setFoundServices(new RunningInstanceMessage[0]);
			else {
				toReturn.setFoundServices(ghnPF.foundServices().toArray(new RunningInstanceMessage[0]));
			}
			if (ghnPF == null || ghnPF.ghns() == null) 
				toReturn.setGhns(new GHN[0]);
			else
				toReturn.setGhns(ghnPF.ghns().toArray(new GHN[0]));
			return toReturn;
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean deployVRE() {
		log.info("--- deployVRE started ---");
		VREGeneratorEvo vreGenerator = null;
		if (isTesting) {
			vreGenerator = new VREGeneratorEvo(getASLSession(), customEPR);
		}
		else {
			ASLSession aslSession = getASLSession();
			vreGenerator = getVREGeneratorEvo(aslSession);
		}	
		try {
			vreGenerator.deployVRE();
			//need time to prepare report
			log.info("--- SLEEP 2 seconds ---");
			Thread.sleep(2500);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}


		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	public ClientDeployReport checkCreateVRE() {
		log.info("--- check Create VRE started ---");

		VREGeneratorEvo vreGenerator = null;
		if (isTesting) {
			try {
				vreGenerator = new VREGeneratorEvo(getASLSession(), customEPR);
			}
			catch (NullPointerException e) {
				log.error("Error while trying to contact VRE Modeler service with TEST ID: " + customEPR +
						" Probly does not exist anymore, request from " + this.getThreadLocalRequest().getRemoteHost() + "("+this.getThreadLocalRequest().getRemoteAddr()+")");
				return new ClientDeployReport();
			}
		}
		else {
			ASLSession aslSession = getASLSession();
			vreGenerator = getVREGeneratorEvo(aslSession);
		}	
		DeployReport report = null;

		try {
			report = vreGenerator.checkVREStatus();
		} catch (Exception e) {
			log.error("Error while trying to retrieve VRE Status, return Empty Report" + e.getMessage() + "\n, " +
					"request from " + this.getThreadLocalRequest().getRemoteHost() + "("+this.getThreadLocalRequest().getRemoteAddr()+")");
			return new ClientDeployReport();
		}

		if (report == null || report.getStatus() == null) {
			log.error("--- DeployReport is NULL or Status is null, return Empty Report, " +
					"request from " + this.getThreadLocalRequest().getRemoteHost() + "("+this.getThreadLocalRequest().getRemoteAddr()+")");
			return new ClientDeployReport();
		}

		if (report.getStatus() == Status.Finished) {
			log.info("--- Create VRE COMPLETED, CREATING LAYOUTS AND COMMUNITY ... ");
			String vreName = "";
			String description = "";
			try {
				VREDescription de = vreGenerator.getVREModel();
				vreName = de.name();
				description = de.description();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			String designer = (String) getASLSession().getAttribute(DESIGNER);
			String manager =  (String) getASLSession().getAttribute(MANAGER);

			log.info("ADDING user designer and manager, found designer: " + designer + " found manager: "+manager);

			long vreCreatedId = createCommunityAndLayoutAndVREFolder(vreName, description, designer, manager);

			log.info("---  createCommunityAndLayout COMPLETED, vreCreatedId="+vreCreatedId);
			String vreScope = "";
			GCubeUser userManager = null;
			if (vreCreatedId > 0) {
				try {
					org.gcube.vomanagement.usermanagement.model.GCubeGroup vreCreated = new LiferayGroupManager().getGroup(vreCreatedId);
					log.info("---  CREATED LAYOUTS AND COMMUNITY OK, updating Calendar Application Profile.");
					vreScope = new LiferayGroupManager().getInfrastructureScope(vreCreatedId);
					String vreUrl = "/group/"+vreCreated.getGroupName().toLowerCase();
					
					boolean newsFeedAppProfileUpdated = false;
					try {
						newsFeedAppProfileUpdated = updateApplicationProfile(NEWS_FEED_APPID, vreScope, vreUrl);  //assumes it is deployed in the home of the VRE
					}
					catch (Exception e) {
						log.error("Something wrong in updateApplicationProfile for " + NEWS_FEED_APPID);
					}
					log.info("updateApplicationProfile for " + NEWS_FEED_APPID + "="+newsFeedAppProfileUpdated);

					UserManager um = new LiferayUserManager();
					GCubeUser userDesigner = um.getUserByUsername(designer);
					userManager = um.getUserByUsername(manager);

					ArrayList<String> toSend = new ArrayList<String>();
					toSend.add(designer);
					String subject = "Definition approved and deployed";
					String body = "Dear "+userDesigner.getFirstName()+", \n\n" + userManager.getFullname() + " has approved the deployment of the group you requested: " + vreName +".";
					body+=".\n\nThis group has been deployed successfully and is already available for you on this portal. Please, check your list.";
					MessageManagerClient client = AbstractPlugin.messages().build();
					String messageId = client.sendMessage(toSend, subject, body, null);
					NotificationsManager nnm = new ApplicationNotificationsManager(
							new SocialNetworkingSite(getThreadLocalRequest()),							
							getASLSession().getScope(),
							new SocialNetworkingUser(getASLSession().getUsername(), getASLSession().getUserEmailAddress(), getASLSession().getUserFullName(), "")
							);
					if (nnm.notifyMessageReceived(designer, messageId, subject, body))
						log.debug("Sending Definition create notification: " + subject + " OK");
				} catch (Exception e) {
					e.printStackTrace();
				}

				log.info("onAfterCreate VRE: " + vreScope);
				long currGroupId = PortalContext.getConfiguration().getCurrentGroupId(getThreadLocalRequest());
				Thread emailManagersThread = new Thread(new NewVRECreatedThread(userManager.getUsername(), userManager.getFullname(), vreScope, vreName, currGroupId));
				emailManagersThread.start();


			} else
				log.error("---  DANGER DANGER DANGER!!!!! -> CREATED LAYOUTS AND COMMUNITY WITH ERRORS");

		}
		else 
			setDeployingStatusOn();

		//jsut for testing
		//		if (isTesting)
		//			return convertServiceDeployReport(simulateReport());

		log.debug("---Sending Report, globalState --- " + report.getStatus() );
		return convertServiceDeployReport(report);
	}
	/**
	 * update the ApplicationProfile of the appid for this scope
	 * @param appId the portlet class name as indicated in the generic resource published at root level 
	 * @param vreScope the scope to add
	 * @param vreURL the absolute URL of the portlet
	 */
	private boolean updateApplicationProfile(String appId, String vreScope, String vreURL) {

		String currScope = ScopeProvider.instance.get();
		String scopeToQuery = PortalContext.getConfiguration().getInfrastructureName();
		ScopeProvider.instance.set("/"+scopeToQuery);

		String endpoint2Add = "<EndPoint><Scope>"+vreScope+"</Scope><URL>"+vreURL+"</URL></EndPoint>";

		SimpleQuery query = queryFor(GenericResource.class);
		query.addCondition("$resource/Profile/SecondaryType/text() eq 'ApplicationProfile'");
		query.addCondition("$resource/Profile/Body/AppId/text() eq '" + appId + "'");

		DiscoveryClient<GenericResource> client = clientFor(GenericResource.class);

		List<GenericResource> list = client.submit(query);
		if (list == null || list.isEmpty()) {
			log.warn("Cannot retrieve the ApplicationProfile from IS for generic resource having  <Body><AppId> = " + appId);
			log.info("Triggering Creation of ApplicationProfile for " + appId);
			GenericResource toCreate = new GenericResource();
			toCreate.newProfile().name("Application Profile for " + appId);
			toCreate.profile().type("ApplicationProfile");
			toCreate.profile().description("Application Profile description for " + appId);
			toCreate.profile().newBody("<AppId>"+appId+"</AppId><ThumbnailURL>No thumbnail</ThumbnailURL>"+endpoint2Add);
			RegistryPublisher rp=RegistryPublisherFactory.create();
			rp.create(toCreate);
			log.info("Creation of ApplicationProfile for " + appId + " OK!");
			return false;
		}

		GenericResource gr = list.get(0);
		log.debug("updating ApplicationProfile for " + gr.profile().name());

		Node fragmentNode;
		try {
			DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Element elem = gr.profile().body();
			fragmentNode = docBuilder.parse(new InputSource(new StringReader(endpoint2Add))).getDocumentElement();
			fragmentNode = elem.getOwnerDocument().importNode(fragmentNode, true);
			elem.appendChild(fragmentNode);
		} catch (Exception e) {
			//in case no xml is entered, just text
			log.error("error");
		}
		RegistryPublisher rp=RegistryPublisherFactory.create();
		rp.update(gr);
		ScopeProvider.instance.set(currScope);
		return true;


	}
	/**
	 * this method is in charge of reflecting the VRE deployed in a gCube Portal context in the HomeLibrary JackRabbit Repository.
		It also creates shared folders for these groups and assign VRE-Managers as Administrators of such foldes 
	 * @param vreName
	 * @param vreScope
	 * @param designer the username of the designer
	 * @param manager the username of the manager
	 * @param description
	 * @throws GroupRetrievalFault 
	 * @throws UserManagementSystemException 
	 */
	private void createVRESharedGroupFolder(UserManager uMan, org.gcube.vomanagement.usermanagement.model.GCubeGroup vreCreated, String designer, String manager, String description) throws Exception {
		GroupManager gm = new LiferayGroupManager();
		String previousToken = SecurityTokenProvider.instance.get();
		String previousUMAToken = AccessTokenProvider.instance.get();
		//get the super user
		log.info("Getting super user with role {}", GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
		//get the super user
		String infraContext = "/"+PortalContext.getConfiguration().getInfrastructureName();
		long rootgroupId = gm.getGroupIdFromInfrastructureScope(infraContext);			
		User theAdmin = LiferayUserManager.getRandomUserWithRole(rootgroupId, GatewayRolesNames.INFRASTRUCTURE_MANAGER);
		if (theAdmin == null) {
			log.error("Cannot create VRE Folder for this new VRE, There is no user having role {} on context: " + GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
		}
		String adminUsername = theAdmin.getScreenName();
		log.info("Got the super user: {}",adminUsername);
		String theAdminToken = PortalContext.getConfiguration().getCurrentUserToken(infraContext, adminUsername);
		List<String> rolesString = new ArrayList<String>();
		List<GCubeRole> theAdminRoles = new LiferayRoleManager().listRolesByUserAndGroup(theAdmin.getUserId(), rootgroupId);			
		for (GCubeRole gCubeRole : theAdminRoles) {
			rolesString.add(gCubeRole.getRoleName());
		}
		rolesString.add(GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
		log.info("legacy authorizationService().setTokenRoles done");
		authorizationService().setTokenRoles(theAdminToken, rolesString);
		SecurityTokenProvider.instance.set(theAdminToken);
		
		OIDCUmaUtil.provideConfiguredPortalClientUMATokenInThreadLocal("/" + PortalContext.getConfiguration().getInfrastructureName());
	
		System.out.println("\n\n***** new authorizationService PortalClient UMA-Token In ThreadLocal done  **** ");
		
		String newVreContext = gm.getInfrastructureScope(vreCreated.getGroupId());
		StorageHubClient shc = new StorageHubClient();
		String vreFolderName = Util.getVREGroupFromContext(newVreContext);
		VREFolderManager vreFolderMaanger = shc.getVreFolderManager(vreFolderName);
		vreFolderMaanger.createVRE(AccessType.WRITE_OWNER, designer);
		vreFolderMaanger.setAdmin(designer);
		if (designer.compareTo(manager) != 0) {
			vreFolderMaanger.addUser(manager);
			vreFolderMaanger.setAdmin(manager);
		}

		log.info("Created the VRE Folder on storageHub: {} ", vreFolderName);

		AccessTokenProvider.instance.set(previousUMAToken);
		SecurityTokenProvider.instance.set(previousToken);
	
		
		//}
	}
	/**
	 * 
	 * @return the html representation of the report
	 */
	public String getHTMLReport() {
		log.info("--- getHTMLReport VRE  ---");

		VREGeneratorEvo vreGenerator = null;
		if (isTesting) {
			vreGenerator = new VREGeneratorEvo(getASLSession(), customEPR);
		}
		else {
			ASLSession aslSession = getASLSession();
			vreGenerator = getVREGeneratorEvo(aslSession);
		}	

		String  report = null;

		try {
			report = Utils.toXML(vreGenerator.checkVREStatus());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		String startDirectory = this.getServletContext().getRealPath("");
		String xslFileLocation = startDirectory + "/styles/report.xsl";

		String transformed = "";
		if (isTesting) {

			String pathXML = startDirectory + "/xml/report.xml";			
			log.info("--- pathXML: " + pathXML);
			log.info("--- xsl: " + xslFileLocation);
			transformed = transformToHtml(fileToString(pathXML), xslFileLocation);
		}
		else {
			transformed = transformToHtml(report, xslFileLocation);
		}

		return transformed;	
	}




	/**
	 * convert the service report to a report client sendable
	 * @param toConvert
	 * @return
	 */
	private ClientDeployReport convertServiceDeployReport(DeployReport toConvert) {

		ClientDeployReport toReturn = new ClientDeployReport();

		//*** Overall deploy status report part
		toReturn.setGlobalStatus(convStatus(toConvert.getStatus()));

		log.trace("***** GHNonCloudReport Not Selected ******");
		ClientCloudReport cdp = new ClientCloudReport();
		cdp.setStatus(DeployStatus.SKIP);
		toReturn.setCloudReport(cdp);

		//*** ResourceManager deploy report part
		toReturn.setResourceManagerReport(
				new ClientResourceManagerDeployingReport(convStatus(toConvert.getResourceDeployingReport().getStatus()), toConvert.getResourceDeployingReport().toString()));

		//*** Functionality deploy report part
		ClientFunctionalityDeployReport cfDeployReport = new ClientFunctionalityDeployReport();


		Hashtable<FunctionalityReport,List<ServiceReport>> table = toConvert.getFunctionalityDeployingReport().getFunctionalityTable();
		HashMap<ClientFunctionalityReport, List<ClientServiceReport>> newTable = new HashMap<ClientFunctionalityReport, List<ClientServiceReport>>(); 

		//creating new hashtable
		for (FunctionalityReport fr : table.keySet()) {
			List<ClientServiceReport> theList = new LinkedList<ClientServiceReport>();			
			for (ServiceReport sr : table.get(fr)) 
				theList.add(new ClientServiceReport(sr.getServiceName(), sr.getServiceClass(), sr.getServiceVersion()));
			//adding new key and payload
			newTable.put(new ClientFunctionalityReport(fr.getFunctionalityId(), fr.getFunctionalityName(), convStatus(fr.getState())), theList);
		}

		cfDeployReport.setStatus(convStatus(toConvert.getFunctionalityDeployingReport().getStatus()));
		cfDeployReport.setReportXML(toConvert.getFunctionalityDeployingReport().getResourceManagerReport());
		cfDeployReport.setFunTable(newTable);
		toReturn.setFunctionalityReport(cfDeployReport);

		//*** Resource deploy report part

		List<ClientResource> newResources = new LinkedList<ClientResource>();	
		for (Resource res : toConvert.getResourceDeployingReport().getResources()) {
			newResources.add(new ClientResource(res.getResourceId(), res.getResourceType(), convStatus(res.getStatus())));
		}

		toReturn.setResourcesReport(new ClientResourcesDeployReport(
				convStatus(toConvert.getResourceDeployingReport().getStatus()), newResources));

		return toReturn;
	}


	/**
	 * 
	 * @param status to convert
	 * @return
	 */
	private DeployStatus convStatus(Status status) {
		switch (status) {
		case Failed:
			return DeployStatus.FAIL;
		case Finished:
			return DeployStatus.FINISH;
		case Pending:
			return DeployStatus.PENDING;
		case Running:
			return DeployStatus.RUN;
		case Skipped:
			return DeployStatus.SKIP;
		case Waiting:
			return DeployStatus.WAIT;
		default:
			return DeployStatus.FAIL;
		}
	}

	/**
	 * 
	 * @param vreName
	 * @return
	 */
	private boolean vreExists(String vreName) {
		GroupManager gm = new LiferayGroupManager();
		try {
			return gm.getGroupId(vreName) >= 0;
		} catch (UserManagementSystemException | GroupRetrievalFault e) {
			return false;
		}
	}

	/**
	 * Creates the community and its layout in Liferay
	 */
	private long createCommunityAndLayoutAndVREFolder(String vreName, String description, String designer, String manager) {

		if (vreExists(vreName)) {
			log.warn("VRE Exists already");
			return -1;
		}

		ASLSession session =  getASLSession();
		log.info("TRYING READING CURRENT VO ID");
		GroupManager gm = new LiferayGroupManager();
		long parentVirtualOrganizationId = -1;
		try {
			parentVirtualOrganizationId = gm.getGroupIdFromInfrastructureScope(session.getScope());
		} catch (IllegalArgumentException | UserManagementSystemException
				| GroupRetrievalFault e1) {
			e1.printStackTrace();
		}
		log.info("CURRENT VO ID="+parentVirtualOrganizationId);

		String desc = (session.getAttribute(DESCRIPTION) == null) ? "No Description found": session.getAttribute(DESCRIPTION).toString(); //desc

		log.info("Designer assigned: " + designer);	
		log.info("Manager assigned : " + manager);

		try {

			String themid = "";
			themid = SiteManagerUtil.getgCubeThemeId(ThemesIdManager.GCUBE_LOGGEDIN_THEME);

			log.info("Assigning theme with id: " + themid + " to VRE in creation");

			GCUBESiteLayout siteLayout = GCubeSiteManagerImpl.getBaseLayout(vreName, false, session.getUsername());
			org.gcube.vomanagement.usermanagement.model.GCubeGroup vreCreated =  gm.createVRE(vreName, parentVirtualOrganizationId, desc);

			long groupId = vreCreated.getGroupId();			
			Group lrGroup = GroupLocalServiceUtil.getGroup(groupId);
			GCubeUser currentVREManagerUserBean = new LiferayUserManager().getUserByUsername(session.getUsername());
			User lrUser = UserLocalServiceUtil.getUser(currentVREManagerUserBean.getUserId());

			SiteManagerUtil.createLayout(lrGroup, lrUser, siteLayout);

			Theme themeToApply = ThemeLocalServiceUtil.getTheme(SiteManagerUtil.getCompany().getCompanyId(), themid, false);
			//update theme
			LayoutSetLocalServiceUtil.updateLookAndFeel(groupId, themeToApply.getThemeId(), "", "", false);
			log.debug("LayoutSet Theme with id " + themid +  " Applied Correctly");

			log.info("Assigned theme with id: " + themid + " to groupid: " + groupId);

			//the method above creates a VRE and assign the manager Role to the person that triggers the creation
			//however the VRE-Designer and the VRE-Manager persons of the VRE could be different and need to be created too
			UserManager uman = new LiferayUserManager();
			//if the manager is not the one who triggered the creation
			String currUser = currentVREManagerUserBean.getUsername();
			log.info("***Username of who triggered the creation is: " + currUser);			

			log.info("Trying to create VRE Group Folder through Storage Hub ...");
			createVRESharedGroupFolder(uman, vreCreated, designer, manager, description);

			log.info("--- createVRESharedGroupFolder OK, \n "
					+ "now assigning the designer and manager to the Site (VRE), sleeping "+ WAITING_TIME_Seconds + " seconds to allow operations on Keycloak ...");
			
			//the creation of groups on Keycloak is asynchronous, if we send an add user to group before the group is actually created on keycloak it would fails, so we wait
			int waitingTime = WAITING_TIME_Seconds / 2 * 1000;
			log.info("\n going in waiting before assing users for " +waitingTime + "  seconds ");
			Thread.sleep(waitingTime);
			log.info("\n still waiting ... " +waitingTime + "  seconds ");
			Thread.sleep(waitingTime);
			log.info("\n still waiting ... " +waitingTime + "  seconds ");
			Thread.sleep(waitingTime);
		
			log.info("\n waiting finished going to assign the users to this VRE");
			RoleManager rm = new LiferayRoleManager();

			if (manager.compareTo(currUser) != 0) {
				//add the role VRE-Manager
				long userId = uman.getUserId(manager);
				uman.assignUserToGroup(groupId, userId);
				log.info("\n\n*** Added selected VRE-Manager " + manager + " to group " + vreName + " with Success");	

				log.info("\n\n*** Now trying assigning Site Role VRE-Manager to " + manager);
				long roleId = rm.getRoleIdByName(VRE_MANAGER);
				rm.assignRoleToUser(userId, groupId, roleId);
				log.info("Site Role VRE-Manager associated to user " + manager +  " with Success");	
			}
			//if the designer is different
			if (designer.compareTo(manager) != 0) {
				log.info("\n\n*** the VRE-Designer is not equal to the VRE-Manager, need to add " + designer + " to this Site (VRE)");	
				//add the role VRE-Designer
				long userId = uman.getUserId(designer);		
				uman.assignUserToGroup(groupId, userId);
				log.info("Added designer " + designer + " to group " + vreName + " with Success");	
				log.info("\n\n*** Now trying assigning Site Role VRE-Designer and then the VRE-Manager to " + designer);
				long roleId = rm.getRoleIdByName(VRE_DESIGNER);
				rm.assignRoleToUser(userId, groupId, roleId);
				log.info("Role VRE-Designer associated to user " + designer);		
				Thread.sleep(1000);
				roleId = rm.getRoleIdByName(VRE_MANAGER);
				rm.assignRoleToUser(userId, groupId, roleId);
				log.info("Role VRE-Manager also associated to user " + designer +  " with Success");	
			}

			return groupId;

		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}

	}
	/**
	 * 
	 * @param profile
	 * @param xslFile
	 * @return
	 */
	private String transformToHtml(String profile, String xslFile){


		File stylesheet  =  new File(xslFile);

		TransformerFactory tFactory = TransformerFactory.newInstance();

		StreamSource stylesource = new StreamSource(stylesheet);

		Transformer transformer = null;
		try {
			transformer = tFactory.newTransformer(stylesource);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			return "";
		}


		Document document = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return "";
		}


		StringReader reader = new StringReader(profile);
		InputSource inputSource = new InputSource(reader);

		log.debug("***** --- Reading **** ");


		try {
			document = builder.parse(inputSource);
		} catch (SAXException e) {
			log.error("***** --- ERROR PARSING REPORT SAXException--- **** ");
			log.error("CHECK THIS: \n" + profile);

			e.printStackTrace();
			return "";
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}


		DOMSource source = new DOMSource(document);

		ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(resultStream);

		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
			return "";
		}

		return resultStream.toString();
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	private String fileToString(String path) {
		BufferedReader filebuf = null;
		String nextStr = null;
		StringBuilder ret = new StringBuilder();
		try {
			filebuf = new BufferedReader(new FileReader(path));
			nextStr = filebuf.readLine(); // Read a line from file
			while (nextStr != null) {
				ret.append(nextStr);
				nextStr = filebuf.readLine(); // Read the next line
			}
			filebuf.close(); // close the file 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return ret.toString();
	}



}

